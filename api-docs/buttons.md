These Buttons are implemented using Angular Material, visit [Angular Material Buttons](https://material.angular.io/components/button/overview) for all functional documentation.

## Imports

```
import { MatButtonModule } from '@angular/material/button';
```

## Disable Ripple

All buttons should use the attribute `disableRipple` to disable the default Angular Material ripple effect on click.

## Properties

| Attribute                         | Description                            | Values              |
| --------------------------------- | -------------------------------------- | ------------------- |
| `@Input() color: ThemePalette`    | Theme color palette for the component. | `primary`, `accent` |
| `@Input() disableRipple: boolean` | Whether ripples are disabled.          |                     |
| `@Input() disabled: boolean`      | Whether the component is disabled.     |                     |

## Types

Primary, secondary, and tertiary styles must all use the base class `cta-button`.

```
<button mat-button disableRipple class="cta-button">
    My button text
</button>
```

| TMO Classifier | Material directive   |
| -------------- | -------------------- |
| Primary        | `mat-flat-button`    |
| Secondary      | `mat-stroked-button` |
| Tertiary       | `mat-button`         |

## Sizes

| Modification | Class              |
| ------------ | ------------------ |
| Small        | `cta-button-small` |
| Large        | `cta-button-large` |

## Length

| Modification | Class         | Description                                                                                  |
| ------------ | ------------- | -------------------------------------------------------------------------------------------- |
| Full-width   | `full-width`  | Full length of container                                                                     |
| Fixed-width  | `fixed-width` | Needed to accommodate caret style. Pair with another class that specifies the width required |

